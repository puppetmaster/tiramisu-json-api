# -*- coding: utf-8 -*-
from json import loads
from os import listdir
from os.path import dirname, abspath, join, normpath, splitext
# import warnings

# from tiramisu.error import ValueWarning
from tiramisu_json_api import Config
from tiramisu_json_api.error import PropertiesOptionError


# warnings.simplefilter("always", ValueWarning)


def list_data(ext='.json'):
    datadir = join(normpath(dirname(abspath(__file__))), 'data')
    filenames = listdir(datadir)
    filenames.sort()
    for filename in filenames:
        if filename.endswith(ext):
            yield join(datadir, filename)


def error_to_str(dico):
    for key, value in dico.items():
        if isinstance(value, list):
            for idx, val in enumerate(value):
                if (isinstance(val, str) and val.startswith('cannot access to')) or isinstance(val, PropertiesOptionError):
                    dico[key][idx] = "PropertiesOptionError"
    return dico


def test_dict():
    debug = False
    # debug = True
    for filename in list_data():
        if debug:
            print('test_jsons', filename)
        with open(filename, 'r') as fh:
            json = loads(fh.read())
        #
        config = Config(json)
        with open(filename[:-4] + 'dict', 'r') as fh:
            dico = loads(fh.read())

        if debug:
            from pprint import pprint
            pprint(dico)
            print('-----')
            pprint(config.value.dict())
        assert error_to_str(dico) == error_to_str(config.value.dict())


def test_get():
    debug = False
    # debug = True
    for filename in list_data():
        if debug:
            print(filename)
        if 'master' in filename:
            continue
        with open(filename, 'r') as fh:
            json = loads(fh.read())
        config = Config(json)
        with open(filename[:-4] + 'dict', 'r') as fh:
            dico = error_to_str(loads(fh.read()))
            for key, value in dico.items():
                if config.option(key).option.isleader():
                    leader_len = len(value)
                if config.option(key).option.isfollower():
                    values = []
                    for index in range(leader_len):
                        val = config.option(key, index).value.get()
                        if isinstance(val, PropertiesOptionError):
                            val = "PropertiesOptionError"
                        values.append(val)
                    assert value == values
                else:
                    assert value == config.option(key).value.get()


def test_owner():
    debug = False
    # debug = True
    for filename in list_data():
        if debug:
            print(filename)
        with open(filename, 'r') as fh:
            json = loads(fh.read())
        config = Config(json)
        with open(filename[:-4] + 'owner', 'r') as fh:
            dico = loads(fh.read())
            for key, value in dico.items():
                if debug:
                    print('key', key)
                if config.option(key).option.isleader():
                    leader_len = len(config.option(key).value.get())
                if config.option(key).option.isfollower():
                    values = {}
                    for index in range(leader_len):
                        try:
                            values[str(index)] = config.option(key, index).owner.get()
                        except PropertiesOptionError:
                            pass
                    assert value == values
                else:
                    assert value == {'null': config.option(key).owner.get()}


def test_prop():
    debug = False
    # debug = True
    for filename in list_data():
        if debug:
            print(filename)
        with open(filename, 'r') as fh:
            json = loads(fh.read())
        config = Config(json)
        with open(filename[:-4] + 'prop', 'r') as fh:
            dico = loads(fh.read())
            for key, value in dico.items():
                if debug:
                    print('key', key)
                for key_, val in value.items():
                    value[key_] = set(val)
                if config.option(key).option.isleader():
                    leader_len = len(config.option(key).value.get())
                if config.option(key).option.isfollower():
                    props = {}
                    for index in range(leader_len):
                        try:
                            props[str(index)] = set(config.option(key, index).property.get())
                        except PropertiesOptionError:
                            pass
                        if 'clearable' in props[str(index)]: 
                            props[str(index)].remove('clearable') 
                else:
                    props = {'null': set(config.option(key).property.get())}
                    if 'clearable' in props['null']: 
                        props['null'].remove('clearable') 
                assert value == props


def test_prop2():
    debug = False
    # debug = True
    for filename in list_data():
        with open(filename, 'r') as fh:
            json = loads(fh.read())
        config = Config(json)
        with open(filename[:-4] + 'prop2', 'r') as fh:
            dico = loads(fh.read())
            for key, value in dico.items():
                if debug:
                    print('key', key)
                for key_, val in value.items():
                    value[key_] = set(val)
                if config.option(key).option.isleader():
                    leader_len = len(config.option(key).value.get())
                if config.option(key).option.isfollower():
                    props = {}
                    for index in range(leader_len):
                        try:
                            props[str(index)] = set(config.option(key, index).property.get(True))
                        except PropertiesOptionError:
                            pass
                        if 'clearable' in props[str(index)]: 
                            props[str(index)].remove('clearable') 
                else:
                    props = {'null': set(config.option(key).property.get(True))}
                    if 'clearable' in props['null']: 
                        props['null'].remove('clearable') 
                assert value == props


def test_info():
    for filename in list_data():
        if 'master' in filename:
            continue
        with open(filename, 'r') as fh:
            json = loads(fh.read())
        config = Config(json)
        with open(filename[:-4] + 'info', 'r') as fh:
            dico = loads(fh.read())
            for key, values in dico.items():
                for info, value in values.items():
                    assert getattr(config.option(key).option, info)() == value, 'error for {} in {}'.format(info, filename)


def test_mod():
    debug = False
    # debug = True
    for filename in list_data('.mod1'):
        if 'master' in filename:
            continue
        if debug:
            print('test_mod', filename)
        with open(filename[:-4] + 'json', 'r') as fh:
            json = loads(fh.read())
        #
        config = Config(json)
        with open(filename) as fh:
            mod = loads(fh.read())
        eval(mod['cmd'])
        #
        if debug:
            from pprint import pprint
            pprint(config.updates)
            print('----------------')
            pprint(mod['body']['updates'])
        assert config.updates == mod['body']['updates']
        #
        with open(filename[:-4] + 'dict1', 'r') as fh:
            dico1 = loads(fh.read())
        assert dico1 == config.value.dict()
        #
        with open(filename[:-4] + 'updates1', 'r') as fh:
            data = loads(fh.read())
            if debug:
                from pprint import pprint
                pprint(data)
            config.updates_data(data)
        assert dico1 == config.value.dict()
