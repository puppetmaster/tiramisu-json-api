#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

import tiramisu_json_api

setup(
        name='tiramisu_json_api',
        version=tiramisu_json_api.__version__,
        packages=find_packages(),
        author="Emmanuel Garette & Tiramisu Team",
        description="A subset of Tiramisu API that works remotly with tiramisu-json",
	long_description=open('README.md').read(),
	include_package_data=True,
	url='https://framagit.org/tiramisu/tiramisu-json-api',

	classifiers=[
	    "Programming Language :: Python",
	    "Development Status :: 1 - Planning",
	    "License :: LGPLv3",
	    "Natural Language :: French",
	    "Operating System :: OS Independent",
	    "Programming Language :: Python :: 3",
	    "Topic :: Communications",
	    ],
)
